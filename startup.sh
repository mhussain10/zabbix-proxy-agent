#!/bin/bash

/usr/bin/docker-agent.sh /usr/sbin/zabbix_agentd --foreground -c /etc/zabbix/zabbix_agentd.conf &
/usr/bin/docker-proxy.sh /usr/sbin/zabbix_proxy --foreground -c /etc/zabbix/zabbix_proxy.conf 
